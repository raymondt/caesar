# caesar

This program is developed to perform a caesar cypher on STDIN, and output the results to STDOUT.

Useful for performing the Caesar cypher on ASCII text files.

-e for encryption
-d for decryption

key  = [1-26]

This program can be installed by downloading and extracting the source files. Opening a terminal in the new directory and typing "sudo make install" or "make" to see the installation options.
