/*
 *  FILE: 		caesar.c
 *  CREATED: 		07 August 2019
 *  AUTHOR: 		Raymond Thomson
 *  CONTACT: 		raymond.thomson76@gmail.com
 *  COPYRIGHT:		Free Use
 */

#define a (int)'a'
#define z (int)'z'
#define A (int)'A'
#define Z (int)'Z'

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int getnextchar(char ins[256]);

int main (int ARGC,char *ARGV[]) {


	// caesar -d 5 < STDIN > STDOUT

	// check program arguments
	if (ARGC == 1 || ARGC == 2) {
		printf("Caesar decrypt/encrypt usage.\ncaesar (-d or -e) 'KEY' < STDIN > STDOUT\n");
		return 1;
	}
	if (ARGC > 3) {
		printf("too many arguments for caesar.\ncaesar (-d or -e)'KEY' < STDIN > STDOUT\n");
		return 2;
	}
	char *s = ARGV[2];
	
	int k = atoi(s);

	if (k > 26 || k < 1) {
		printf("key has to be between 1 and 26.\n");
		return 3;
	}

	char ch; 		// current buffer character
	char buf[BUFSIZ];	// STDIN buffer
	int bc;			// current buffer read count
	int opt = 0;		// options for encrypt or decrypt

	// set and check encrypt or decrypt option
	if (strstr(ARGV[1],"-e") != NULL) opt = 1;
	if (strstr(ARGV[1],"-d") != NULL) opt = 2;
	if (opt == 0) {	
		printf("you must add -d or -e to decrypt or encrypt.\ncaesar (-d or -e) 'KEY' < STDIN > STDOUT\n");
		return 4;
	}

	while ((bc = read(0, buf, BUFSIZ)) > 0) {
		for (int i = 0;i < bc;i++) {
			ch = buf[i];
			if (opt == 1) {
				if (ch >= a && ch <= z) {
					buf[i] = (((ch-a)+k)%26)+a;
				} else if (ch >= A && ch <= Z) {
					buf[i] = (((ch-A)+k)%26)+A;
				}
			}
			if (opt == 2) {
				if (ch >= a && ch <= z) {
					buf[i] = (((ch-a)-k)%26)+a;
				} else if (ch >= A && ch <= Z) {
					buf[i] = (((ch-A)-k)%26)+A;
				}
			}
		}
		write(1, buf, bc);
	}

	return 0;
}
