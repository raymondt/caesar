# Application : caesar cypher cli program
#	Description
# Author Raymond Thomson

all: help

CC=gcc
MAN=man/
SRC=src/
CFLAGS=-I.
LIBS=-lm

%.o: %.c
	@echo 'Invoking: GCC C object files'
	$(CC) -c -o $($SRC)$@ $< $(CFLAGS)

caesar: $(SRC)caesar.o
	@echo 'Building and linking target: $@'
	$(CC) -o caesar $(SRC)*.o $(LIBS)

local: caesar clean

clean:
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	@echo 'Installed. Enter ./caesar to run'

install: caesar 
	@echo 'Installing'	
	cp caesar /usr/local/bin/caesar
	cp $(MAN)caesar /usr/share/man/man1/caesar.1
	gzip /usr/share/man/man1/caesar.1
	@echo 'Cleaning build objects'
	rm -f $(SRC)*.o 
	rm -f caesar
	@echo 'installed, type caesar to run or man caesar for the manual'

remove:
	rm -f caesar

uninstall:
	rm -f /usr/local/bin/caesar
	rm -f /usr/share/man/man1/caesar.1.gz
	@echo 'caesar uninstalled.'

help:
	@echo 'Make options for caesar'
	@echo 'Local Folder make'
	@echo '    make local'
	@echo 'Local Folder uninstall'
	@echo '    make remove'
	@echo 'System Install'
	@echo '    sudo make install'
	@echo 'System Uninstall'
	@echo '    sudo make uninstall'
	
